#
# MAIN
#
alias lll='ls -haltrF'
alias ll='ls --color=auto -hla'
alias l=ll
alias mkp='mkdir -p'
alias UP='nice -n 11 apt-fast update; nice -n 11 apt-fast dist-upgrade -y && nice -n 11 apt-fast autoremove -y && nice -n 11 snap refresh && flatpak update -y -v'
alias up-install='apt-fast update && apt-fast install -y'
alias bman='man --html=x-www-browser'
alias ke="komodo-edit"
alias test-mail="echo 'body <br/>' | mail -s 'Subject: Test mail' test@localhost"
alias high_fan="sudo echo '255' > /sys/class/hwmon/hwmon2/pwm1"
alias medium_fan="sudo echo '155' > /sys/class/hwmon/hwmon2/pwm1"
alias low_fan="sudo echo '100' > /sys/class/hwmon/hwmon2/pwm1"
alias low_brightness="sudo echo '200' > /sys/class/backlight/intel_backlight/brightness"
alias medium_brightness="sudo echo '600' > /sys/class/backlight/intel_backlight/brightness"
alias high_brightness="sudo echo '1200' > /sys/class/backlight/intel_backlight/brightness"
alias max_brightness="sudo echo '4800' > /sys/class/backlight/intel_backlight/brightness"
alias uncompress="dtrx"
alias vimenc="vim -u ~/.encrypted_vim_rc "
alias vide="vim -u ~/.vimrc_ide "
alias hosts='cat /etc/hosts'
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias untar="tar xzvkf"
alias ip4="ip -4 addr"
alias ip6="ip -6 addr"
alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'

#
# WEB
#
alias wp="wp-cli.phar"
alias composer-redeploy="composer.phar run-script post-install-cmd -vvv -- --redeploy"
alias getHttpCode='curl -s -o /dev/null -w "%{http_code}" '
alias flushRedis='redis-cli -a "$REDIS_LOCAL_PASSWORD" FLUSHALL;'
alias redis-clean='redis-cli -a "$REDIS_LOCAL_PASSWORD" FLUSHALL;'
alias rls='sudo mount -B $HOME/var/data/mysql/ /var/lib/mysql && sudo systemctl restart $WEB_LOCAL_DAEMONS'
alias sls='sudo systemctl status $WEB_LOCAL_DAEMONS'

#
# MAGENTO
#
alias set_magento_file_permissions="find ./ -type f | xargs chmod 644 && find ./ -type d | xargs chmod 755 && chmod -Rf 777 var && chmod -Rf 777 media"
alias sle="cp ~/Templates/magento/.htaccess .htaccess_dev && cp ~/Templates/magento/zzz_local.xml app/etc/zzz_local_dev.xml && cp ~/Templates/magento/Zzz_LocallyDisabledModules.xml app/etc/modules/zzz_disabled_modules.xml"
alias add_backend_user="n98-magerun.phar admin:user:create -n admin $(git config --global user.email) root $(git config --global user.name | awk '{print $1;}' ) $(git config --global user.name | awk '{print $2;}' )"

#
# GIT
#
alias gs="git status --short"
alias gst="gs"
alias ga="git add ."
alias gadd="ga"
alias gd="git diff HEAD"
alias gdiff="gd"
alias gl="git log"
alias gll="git log --graph --oneline --decorate --all --color"
alias glf="git diff-tree --no-commit-id --name-only -r "
alias glt="get_last_git_tag"

#
# HG
#
alias hg="python2 /usr/bin/hg"
alias hst="hg status"
alias hs="hg status"
alias hadd="hg addremove"
alias ha="hg addremove"
alias hdiff="hg diff"
alias hd="hg diff"
alias hl="hg log | less"
alias hll="hg log --graph | less"
alias hlf="hs --change"


#
# DOCKER
#
alias ds="echo -ne '\n --- IMAGES --- \n\n' && docker images --all && echo -ne '\n --- NETWORK --- \n\n' && docker network ls && echo -ne ' --- CONTAINERS --- \n\n' && docker ps -a "
alias drmc="docker rm -f $(docker ps -aq)"
alias drmi="docker rmi -f $(docker images -q)"
alias drmi-none="docker rmi -f $(docker images -a | grep '<none>' | awk '{print $3}')"
alias dex="docker exec -it"
alias dcb="docker-compose build"
alias dcu="docker-compose up"
alias dcd="docker-compose down --rmi local --remove-orphans"
alias dcl="docker system prune -f"

#
# PODMAN
#
#alias docker="sudo podman"

#
# KUBERNETES
#
alias k="kubectl "
alias kst="kubectl get no,ns,all --all-namespaces "
alias kwa="toolboxctl main exec watch -n3 kubectl get no,ns,all --all-namespaces"
alias kwe="toolboxctl main exec kubectl get events -w --all-namespaces"

#
# HELM
#
alias helmf="helm fetch -d $HOME/Projects/kubernetes/charts --untar"

#
# SECURITY
#
alias start_owasp_zap="docker run -u zap -p 8080:8080 -p 8090:8090 -i owasp/zap2docker-stable zap-webswing.sh"
alias op-sign-in="export OP_TOKEN_PATH=$HOME/.op_session_token; $(which op-sign-in) && source $HOME/.op_session_token; rm $HOME/.op_session_token"
alias encrypt='gpg --encrypt --trust-model always -r $GPG_CRYPT_USER '
alias decrypt='gpg --decrypt --armor -r $GPG_CRYPT_USER '

#
# Testing
#
#alias cypress='xhost local:root; docker run --rm -it --net host --shm-size 1g -v $PWD:/e2e -w /e2e --entrypoint=cypress -v /tmp/.X11-unix:/tmp/.X11-unix -v /tmp/:/tmp/ -e DISPLAY=unix$DISPLAY docker.io/cypress/included:3.8.1'
alias cypress='xhost local:root; sudo podman run --rm -it -u1000:1000 --net host --shm-size 1g -v $PWD:/src -w /src --entrypoint=cypress -v /tmp/.X11-unix:/tmp/.X11-unix -v /tmp/:/tmp/ -v /var/run/dbus/system_bus_socket:/var/run/dbus/system_bus_socket -e DISPLAY=unix$DISPLAY docker.io/cypress/included:4.1.0'
