"" Protect changes between writes. Default values of
"" updatecount (200 keystrokes) and updatetime
"" (4 seconds) are fine
""set swapfile
"set directory^=~/.vim/swap//
"
"" protect against crash-during-write
"set writebackup
"" but do not persist backup after successful write
"set nobackup
"" use rename-and-write-new method whenever safe
"set backupcopy=auto
"" patch required to honor double slash at end
"if has("patch-8.1.0251")
"	" consolidate the writebackups -- not a big
"	" deal either way, since they usually get deleted
"	set backupdir^=~/.vim/backup//
"end
"
"" persist the undo tree for each file
"set undofile
"set undodir^=~/.vim/undo//

" Nerdtree configuration
"
" open a NERDTree automatically when vim starts up if no files were
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" open NERDTree automatically when vim starts up on opening a directory
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

