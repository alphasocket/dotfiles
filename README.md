# Dotfiles notes

## Install dotfiles
~~~
git clone git@gitlab.com:alphasocket/dotfiles ~
git submodule update --init --recursive
~~~

## Install a project
~~~
git submodule add -f git@$PROVIDER:$AUTHOR/$PROJECT ~/Projects/$AUTHOR/$PROJECT
git submodule add -f git@gitlab.com:alphasocket/ansible ~/Projects/alphasocket/ansible
~~~

## Update
~~~
git -C $HOME pull -j10 --recurse-submodules 
~~~
